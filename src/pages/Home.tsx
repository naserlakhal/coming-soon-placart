import {
  faFacebook,
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import logo from '../assets/img/logo_solid.png';
import {
  faChartLine,
  faInbox,
  faLightbulb,
  faLocation,
  faMailBulk,
  faPhone,
  faUser,
} from '@fortawesome/free-solid-svg-icons';
import { CSSProperties } from 'react';

function Home() {
  const boldText: CSSProperties = {
    fontWeight: 'bold',
  };
  return (
    <>
      <div id="header">
        <div className="container d-flex flex-column align-items-center">
          <img src={logo} width={'300'} alt="" />
          <h2>
            Nous sommes actuellement en plein travail pour perfectionner notre
            site web et serons prêts à le lancer très prochainement.
          </h2>
          <div
            className="countdown d-flex justify-content-center"
            style={{ color: 'white' }}
          >
            <div>
              <h3>0</h3>
              <h4>Jours</h4>
            </div>
            <div>
              <h3>0</h3>
              <h4>Heures</h4>
            </div>
            <div>
              <h3>0</h3>
              <h4>Minutes</h4>
            </div>
            <div>
              <h3>0</h3>
              <h4>Secondes</h4>
            </div>
          </div>

          <div className="social-links text-center">
            <a href="#" className="twitter">
              <FontAwesomeIcon icon={faTwitter} />
            </a>
            <a href="#" className="facebook">
              <FontAwesomeIcon icon={faFacebook} />
            </a>
            <a href="#" className="instagram">
              <FontAwesomeIcon icon={faInstagram} />
            </a>
            <a href="#" className="linkedin">
              <FontAwesomeIcon icon={faLinkedinIn} />
            </a>
          </div>
        </div>
      </div>
      <div id="main">
        <section id="about" className="about">
          <div className="container">
            <div className="section-title">
              <h2>QUI SOMMES NOUS:</h2>
              <p>
                Depuis 2000*, notre société crée et fabrique des dressings et
                placards sur mesure. Avec son équipe, Plac’art s’engage à vous
                apporter des réponses à vos besoins avec des produits INNOVANTS,
                FONCTIONNELS et de GRANDE QUALITÉ.
              </p>
            </div>

            <div className="row mt-2">
              <div className="col-lg-4 col-md-6 icon-box">
                <div className="icon">
                  <FontAwesomeIcon icon={faLightbulb} />
                </div>
                <h4 className="title">
                  <p>CONCEPTION</p>
                </h4>
                <p className="description">
                  Une équipe de formation consolidée dans le métier, disponible
                  pour conseiller le client afin d'offrir des conceptions
                  alliant fonctionnalité, qualité et esthétique.
                </p>
              </div>
              <div className="col-lg-4 col-md-6 icon-box">
                <div className="icon">
                  <FontAwesomeIcon icon={faChartLine} />
                </div>
                <h4 className="title">
                  <p>FABRICATION</p>
                </h4>
                <p className="description">
                  Une unité, en progression continue, nous permet de proposer
                  des produits conformes aux exigences et aux attentes de nos
                  clients.
                </p>
              </div>
              <div className="col-lg-4 col-md-6 icon-box">
                <div className="icon">
                  <FontAwesomeIcon icon={faUser} />
                </div>
                <h4 className="title">
                  <p>POSE</p>
                </h4>
                <p className="description">
                  Plac'Art dispose d'une équipe de pose professionnelle, formée,
                  outillée et très impliquée dans le processus de qualité.
                </p>
              </div>
            </div>
          </div>
        </section>
        <section id="contact" className="contact">
          <div className="container">
            <div className="section-title">
              <h2>CONTACTEZ-NOUS</h2>
            </div>

            <div className="row">
              <div className="col-lg-5 d-flex align-items-stretch">
                <div className="info">
                  <div className="address">
                    <i>
                      <FontAwesomeIcon icon={faLocation} />
                    </i>
                    <h4>Nous retrouver:</h4>
                    <p>
                      Show-room Soukra: 58, Avenue Fatouma Bourguiba-2036 La
                      Soukra
                    </p>

                    <p>
                      Show-room Ariana: 65, Avenue Habib Bourguiba-2080 Ariana
                    </p>
                  </div>

                  <div className="email">
                    <i>
                      <FontAwesomeIcon icon={faMailBulk} />
                    </i>
                    <h4>Email:</h4>
                    <p>contact@plac-art.tn</p>
                  </div>

                  <div className="phone">
                    <i>
                      <FontAwesomeIcon icon={faPhone} />
                    </i>
                    <h4>Call:</h4>
                    <p>
                      <span style={boldText}>Tel </span>: 70 69 18 87(Soukra){' '}
                    </p>
                    <p>
                      <span style={boldText}>Fax</span> : 70 69 18 87(Soukra){' '}
                    </p>
                    <p>
                      {' '}
                      <span style={boldText}>Tel </span> : 71 70 55 90(Ariana ){' '}
                    </p>
                    <p>
                      <span style={boldText}>Fax</span> : 71 71 62 79(Ariana ){' '}
                    </p>
                    <p>
                      <span style={boldText}>Mobile</span> : 24 91 00 91(Ariana
                      )
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3807.812762499799!2d10.27261011932954!3d36.87318369665831!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12e2b569ef9d4727%3A0x28ab22ba73733dde!2sPlac&#39;Art!5e0!3m2!1sen!2sus!4v1704562889571!5m2!1sen!2sus"
                  style={{ border: '0', width: '100%', height: '100%' }}
                ></iframe>
              </div>
            </div>
          </div>
        </section>
      </div>
    </>
  );
}

export default Home;
